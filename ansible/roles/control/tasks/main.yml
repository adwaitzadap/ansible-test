---
- name: Install epel from remote repo on CentOS 7
  yum: name=https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm state=present
  when: ansible_os_family == 'RedHat'
 
- name: Install remi from remote repo on CentOS 7
  yum: name=http://rpms.remirepo.net/enterprise/remi-release-7.rpm state=present
  when: ansible_os_family == 'RedHat'

- name: Install libselinux-python binary for Ansible to work
  yum: name=libselinux-python state=present
  when: ansible_pkg_mgr == "yum"

- name: Install Stuff on centOS
  yum: name={{item}} state=installed update_cache=yes
  with_items:
    - redis
    - varnish
    - nginx
    - nginx-all-modules
    - php-cli
    - php-mysql
    - php70-php-fpm
    - php70-php-devel
    - php70-php-gd
    - php70-php-cli
    - php70
    - MySQL-python
    - php70-php-json
    - php70-php-mbstring  
    - php70-php-mcrypt
    - php70-php-mysqlnd
    - php70-php-opcache
    - php70-php-pecl-memcached
    - php70-php-pecl-memcache
    - php70-php-pecl-redis
    - mariadb-server
#    - Percona-Server-server-57
  when: ansible_os_family == 'RedHat'

- name: install tools
  apt: name={{item}} state=present update_cache=yes
  with_items:
    - python-httplib2
    - python-mysqldb
  when: ansible_os_family == 'Debian'

- name: install Stuff on Ubuntu
  apt: name={{item}} state=present update_cache=yes
  with_items:
    - nginx
    - php7.0-fpm
    - php7.0-curl
    - php7.0-cli
    - php7.0-mbstring
    - php7.0-mysql
    - redis-server
    - percona-server-server-5.6
    - varnish
  when: ansible_os_family == 'Debian'

- name: creating database
  mysql_db: name={{ database_name }} state=present

- name: creating database user
  mysql_user: name={{ database_user }} password={{ database_pass }} priv={{ database_name }}.*:ALL host='localhost' state=present

- name: de-activate default site
  file: path=/etc/nginx/sites-enabled/default state=absent
  notify: restart nginx
  when: ansible_os_family == 'Debian'

- name: copying varnish.service
  template: src=varnish.service.j2 dest=/lib/systemd/system/varnish.service mode=0644
  when: ansible_os_family == 'Debian' 

- name: copying varnish default.vcl
  template: src=default.vcl.j2 dest=/etc/varnish/default.vcl mode=0644

- name: copying varnish.params on centOS
  template: src=varnish.params.j2 dest=/etc/varnish/varnish.params mode=0644
  when: ansible_os_family == 'RedHat' 

- name: copying varnish file
  template: src=varnish.j2 dest=/etc/default/varnish mode=0644
  notify: restart varnish
  when: ansible_os_family == 'Debian'
  
- name: Stopping varnish temporarily
  shell: systemctl daemon-reload
  #notify: restart varnish
  when: ansible_os_family == 'Debian'

- name: configure nginx sites
  template: src=nginx.conf.j2 dest=/etc/nginx/nginx.conf mode=0644
  notify: restart nginx
  when: ansible_os_family == 'Debian'

- name: configure nginx sites centOS
  template: src=nginx.conf.centos.j2 dest=/etc/nginx/nginx.conf mode=0644
  notify: restart nginx
  when: ansible_os_family == 'RedHat'

- name: create linux user 
  user: name={{ system_user }}

- name: creating webdirectory path
  file: path=/home/{{ system_user }}/{{ domain_name }} state=directory

- name: configure nginx sites on ubuntu
  template: src=nginx-site.conf.j2 dest=/etc/nginx/sites-available/{{ domain_name }}.conf mode=0644
  notify: restart nginx
  when: ansible_os_family == 'Debian'

- name: configure nginx sites on centOS 
  template: src=nginx-site-centos.conf.j2 dest=/etc/nginx/conf.d/{{ domain_name }}.conf mode=0644
  notify: restart nginx
  when: ansible_os_family == 'RedHat'

#- name: get active sites
#  shell: ls -1 /etc/nginx/sites-enabled
#  register: active

- name: activate nginx sites
  file: src=/etc/nginx/sites-available/{{ domain_name }}.conf dest=/etc/nginx/sites-enabled/{{ domain_name }}.conf state=link
  notify: restart nginx
  when: ansible_os_family == 'Debian'

- name: ensure nginx started
  service: name=nginx state=restarted enabled=yes
#  when: ansible_os_family == 'Debian'

#- name: ensure nginx started on centOS
#  shell: systemctl restart nginx
#  when: ansible_os_family == 'Redhat'

- name: ensure mysql started
  service: name=mysql state=started enabled=yes
  when: ansible_os_family == 'Debian'

- name: ensure mysql started on centOS
  service: name=mariadb state=started enabled=yes
  when: ansible_os_family == 'RedHat'

#- name: ensure mysql started on centOS
#  shell: systemctl restart mysql
#  when: ansible_os_family == 'RedHat'

- name: ensure varnish started
  service: name=varnish state=restarted enabled=yes
#  when: ansible_os_family == 'Debian'

#- name: ensure varnish started on centOS
#  shell: systemctl restart varnish
#  when: ansible_os_family == 'RedHat'

- name: ensure php7 started
  service: name=php7.0-fpm state=started enabled=yes
  when: ansible_os_family == 'Debian'

- name: ensure php7 started on centOS
  service: name=php70-php-fpm state=restarted enabled=yes
  when: ansible_os_family == 'RedHat'

- name: ensure redis started
  service: name=redis-server state=started enabled=yes
  when: ansible_os_family == 'Debian'

- name: ensure redis started on centOS
  service: name=redis state=restarted enabled=yes
  when: ansible_os_family == 'RedHat'

- name: setup WP-CLI 
  copy: src=files/wp dest=/bin/ mode=0755

- name: Download Wordpress
  shell: wp core download --allow-root --path=/home/{{ system_user }}/{{ domain_name }}/

- name: setup WordPress wp-config.php file
  shell: wp core config  --allow-root --dbname={{ database_name }} --dbuser={{ database_user }} --dbpass={{ database_pass }} --path=/home/{{ system_user }}/{{ domain_name }}/

- name: Installing WordPress 
  shell: wp core install --allow-root --url={{ domain_name }} --title={{ domain_name }} --admin_user={{ wp_admin_user }} --admin_password={{ wp_admin_pass }} --admin_email={{ wp_admin_email }} --path=/home/{{ system_user }}/{{ domain_name }}/
#- name: Download WordPress
#  get_url: url=https://wordpress.org/latest.tar.gz 
#    dest=/tmp/wordpress.tar.gz
#    validate_certs=no 

#- name: Extracting Wordpress 
#  unarchive: src=/tmp/wordpress.tar.gz dest=/tmp/ copy=no

#- name: Moving WP files 
#  shell: mv /tmp/wordpress/* /home/{{ system_user }}/{{ domain_name }}/

- name: Giving correct permissions
  file: path=/home/{{ system_user }}/{{ domain_name }} owner=www-data group=www-data recurse=yes
  when: ansible_os_family == 'Debian'

- name: Giving correct permissions in centOS
  file: path=/home/{{ system_user }}/{{ domain_name }} owner=apache group=apache recurse=yes
  when: ansible_os_family == 'RedHat'

- name: user home dir permission fix
  file: path=/home/{{ system_user }} group=apache mode=770
  when: ansible_os_family == 'RedHat'
